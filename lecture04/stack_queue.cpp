// stack_queue.cpp

#include "stack_queue.hpp"

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
    stack_queue <int> s;

    s.push(2);
    cout << s.front() << endl;
    s.push(3);
    cout << s.front() << endl;
    s.push(1);
    cout << s.front() << endl;
    s.pop();
    cout << s.front() << endl;
    s.pop();
    cout << s.front() << endl;

    return 0;
}
